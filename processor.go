package queue

// CallbackHandler to process unmarshalled data
type CallbackHandler func(interface{}) error

// Processor processing data from beanstalkd
type Processor interface {
	DoProcess(data interface{}, handler CallbackHandler) error
}
