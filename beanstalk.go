package queue

import "github.com/iwanbk/gobeanstalk"

const (
	// PriorityLow Low priority
	PriorityLow = uint32(2048)
	// PriorityMedium Medium priority
	PriorityMedium = uint32(1024)
	// PriorityHigh High priority
	PriorityHigh = uint32(512)
	// PriorityHighest Highest priority
	PriorityHighest = uint32(256)
	// PriorityImmediately Immediately priority
	PriorityImmediately = uint32(10)
)

// Beanstalk bla bla
type Beanstalk struct {
	host       string
	connection *gobeanstalk.Conn
	protocol   Protocol
	processor  Processor
}

// Connect connect to beanstalkd
func (b *Beanstalk) Connect() error {
	con, err := gobeanstalk.Dial(b.host)
	if err != nil {
		return err
	}

	b.connection = con
	return nil
}

// Close beanstalkd connection
func (b *Beanstalk) Close() {
	if b.connection != nil {
		b.connection.Quit()
	}
}
