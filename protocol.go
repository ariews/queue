package queue

// Protocol interface (decode and encode data)
type Protocol interface {
	Decode(byte []byte) (interface{}, error)
	Encode(data interface{}) ([]byte, error)
}
