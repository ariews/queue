package queue

import (
	"time"
)

// Producer struct
type Producer struct {
	Beanstalk
}

// Store the job to beanstalkd
func (p *Producer) Store(data interface{}, priority uint32) (uint64, error) {
	body, err := p.protocol.Encode(data)
	if err != nil {
		return 0, err
	}

	delay := 0 * time.Second
	timeToRun := 20 * time.Second

	return p.connection.Put(body, priority, delay, timeToRun)
}

// UseTube set tube
func (p *Producer) UseTube(tube string) error {
	return p.connection.Use(tube)
}

// StoreJob useTube and store job
func (p *Producer) StoreJob(job Job, priority uint32) (uint64, error) {
	if err := p.UseTube(job.TubeName()); err != nil {
		return 0, err
	}

	return p.Store(job, priority)
}

// MakeProducer create producer
func MakeProducer(host string, protocol Protocol) *Producer {
	return &Producer{
		Beanstalk: Beanstalk{
			host:     host,
			protocol: protocol,
		},
	}
}
