package queue

import (
	"github.com/iwanbk/gobeanstalk"
)

// WorkerErrHandler worker error handler
type WorkerErrHandler func(w *Worker, job *gobeanstalk.Job, err error)

// Worker struct
type Worker struct {
	Beanstalk
	errHandler WorkerErrHandler
}

// SetErrorHandler set worker error handler
func (w *Worker) SetErrorHandler(errHandler WorkerErrHandler) {
	w.errHandler = errHandler
}

// ProcessJob process the job
func (w *Worker) ProcessJob(callback CallbackHandler) {
	job, err := w.connection.Reserve()
	if err != nil {
		w.errHandler(w, job, err)
		return
	}

	data, err := w.protocol.Decode(job.Body)
	if err != nil {
		w.errHandler(w, job, err)
		return
	}

	err = w.processor.DoProcess(data, callback)
	if err != nil {
		w.errHandler(w, job, err)
		return
	}

	_ = w.connection.Delete(job.ID)
}

// Bury the job
func (w *Worker) Bury(jobID uint64, priority uint32) error {
	return w.connection.Bury(jobID, priority)
}

// Watch tube
func (w *Worker) Watch(tube string) (int, error) {
	if w.connection == nil {
		if err := w.Connect(); err != nil {
			return 0, err
		}
	}

	return w.connection.Watch(tube)
}

// MakeWorker create worker
func MakeWorker(host string, protocol Protocol, processor Processor) *Worker {
	return &Worker{
		errHandler: DefaultWorkerErrHandler,
		Beanstalk: Beanstalk{
			host:      host,
			protocol:  protocol,
			processor: processor,
		},
	}
}

// DefaultWorkerErrHandler default err handler
func DefaultWorkerErrHandler(w *Worker, job *gobeanstalk.Job, err error) {
	_ = w.Bury(job.ID, uint32(20))
}
